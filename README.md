[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17570914%2Fissues_statistics)](https://gitlab.com/containerizeme/nodered/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17570914%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/containerizeme/nodered/-/commits/master)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17570914%3Flicense%3Dtrue)](https://gitlab.com/containerizeme/nodered/-/blob/master/LICENSE)
applies to software of this project. The running software within the image/container ships with its own license(s).

[![Node-RED](https://badgen.net/badge/project/Node-RED/orange?icon=gitlab)](https://gitlab.com/containerizeme/nodered/-/blob/master/README.md#node-red)
[![Stable Version](https://img.shields.io/docker/v/icebear8/nodered/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/nodered/-/blob/master/CHANGELOG.md#node-red)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/nodered?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/nodered)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/nodered/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/nodered)

# Node-RED
[Node-RED](https://nodered.org/) flow-based programming for the Internet of Things

This image contains a basic installation of Node-RED.
The image is prepared to access remote git repositories as well.

# Usage
`docker run -p 1880:1880 icebear8/nodered`

# Configuration and setup
* Use the built-in git support from Node-RED.
* At each container start a git user and ssh key is created if not available
* The newly created key is printed to the log at startup

##  Available volumes
| VOLUME              | Description |
|-                    |-            |
| ${APP_RUNTIME_DIR}  | Contains the Node-RED runtime configuration and data |
| ${GIT_CONFIG_DIR}   | Contains the git configuration and ssh keys |
