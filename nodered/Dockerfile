# Onbuild parameters for git repo utils
ARG USER=Ahab
ARG GROUP=Ahab
ARG UID=1000
ARG GID=1000
ARG REPO_DIR=/opt/repo
ARG NODERED_VERSION=4.0.8

ARG BASE_IMG_VERSION=3.21.0-r1-onbuild

FROM icebear8/gitrepoutils:${BASE_IMG_VERSION}

# APP_INSTALL_DIR: Node-red zip creates a 'node-red' folder => /opt/node-red
ENV APP_RUNTIME_DIR=/srv/nodered
ARG APP_INSTALL_DIR=/opt/node-red

# Application tools and helpers
ENV APP_UTILS_DIR=/opt/utils/app
ENV APP_LAUNCHER=${APP_UTILS_DIR}/launcher.sh

# Default arguments for service execution
ENV SERVICE_ARGS="--userDir ${APP_RUNTIME_DIR}"

RUN apk update && apk add --no-cache \
  nodejs=22.11.0-r0 \
  npm=10.9.1-r0

# Install node-red
RUN mkdir -p ${APP_INSTALL_DIR}
ENV PATH="${APP_INSTALL_DIR}/bin:${PATH}"
RUN npm install --prefix ${APP_INSTALL_DIR} -g node-red@${NODERED_VERSION}


# Prepare the directory for the application data and configuration
# Copy the default settings as well
RUN mkdir -p ${APP_RUNTIME_DIR}
COPY ./resources/settings.js ${APP_RUNTIME_DIR}

# Prepare launcher utilities
RUN mkdir -p ${APP_UTILS_DIR}
COPY /utils/* ${APP_UTILS_DIR}/

# Fix access rights
RUN chmod -R 755 ${APP_UTILS_DIR}/*.sh && \
    chown -R ${USER}:${GROUP} ${APP_RUNTIME_DIR}

USER ${USER}
EXPOSE 1880
VOLUME ${APP_RUNTIME_DIR}

ENTRYPOINT ["sh", "-c"]
CMD ["sh ${APP_LAUNCHER}"]
