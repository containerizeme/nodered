# Node-RED

### 4.0.8-r1
* Update Node-Red 4.0.8
* Update Alpine 3.21.0
  ** nodejs 22.11.0-r0
  ** npm 10.9.1-r0

### 4.0.5-r1
* Update Node-Red 4.0.5

### 4.0.2-r1
* Update Node-Red 4.0.2
* Update base image gitrepoutils:3.20.3-r1
* nodejs 20.15.1-r0
* npm 10.8.0-r0

### 3.1.9-r1
* Update Node-Red 3.1.9
* Update base image gitrepoutils:3.19.1-r3

### 3.1.8-r1
* Update Node-Red 3.1.8
* Update Alpine 3.19.1
* nodejs 20.12.1-r0

### 3.1.3-r1
* Update Node-Red 3.1.3
* Update Alpine 3.19.0
* nodejs 20.10.0-r1 \
* npm 10.2.5-r0

### 3.1.0-r2
* Startup fix
* Update Node-RED 3.1.0
* Update base image gitrepoutils:3.18.4-r1 (Alpine 3.18.4)
* nodejs 18.17.1-r0

### 3.1.0-r1
* obsolete, do not use

### 3.0.2-r5
* Update base image to Alpine 3.18.0 (3.18.0-r1-onbuild)
* Update pinned versions
  ** git 2.40.1-r0
  ** nodejs 18.16.0-r1
  ** npm 9.6.6-r0

### 3.0.2-r4
* Update base image (3.17.3-r2 with git 2.38.5-r0)
* Pinned nodejs version 18.14.2-r0
* Pinned npm version 9.1.2-r0

### 3.0.2-r3
* Update base image gitrepoutils:3.17.3-r1 (Alpine 3.17.3)

### 3.0.2-r2
* Update base image gitrepoutils:3.17.2-r1-onbuild (Alpine 3.17.2)

### 3.0.2-r1
* Update NodeRed 3.0.2
* Update base image gitrepoutils:3.17.0-r1-onbuild (Alpine 3.17.0)

### 3.0.1-r1
* Update NodeRed to 3.0.1
* Update base image gitrepoutils:3.16.1-r1-onbuild (Alpine 3.16.1)

### 2.2.2-r3
* Update base image gitrepoutils:3.15.0-r2-onbuild
* Update expat to 2.4.5-r0

### 2.2.2-r2
* obsolete, do not use

### 2.2.2-r1
* Update NodeRed 2.2.2

### 2.1.4-r1
* Update NodeRed 2.1.4
* Update Alpine 3.15.0

### 2.1.3-r1
* Update NodeRed 2.1.3
* Update Alpine 3.14.3

### 2.0.6-r2
* Enable 'Project' featur in settings by default

### 2.0.6-r1
* Fix nodered setup
* Update Node-RED 2.0.6
* Update Alpine 3.14.2

### 2.0.6-r0
* DO NOT USE, node-RED not properly installed
* Update Node-RED 2.0.6
* Update Alpine 3.14.2

### 1.3.5-r1
* Update Node-RED 1.3.5
* Update Alpine 3.13.5

### 1.2.9-r1
* Update Node-RED 1.2.9
* Update Alpine 3.13.2

### 1.0.6-r1
* Update Node-RED to 1.1.3
* Update Alpine (3.12.0)
### 1.0.6-r1
* Update Node-RED to 1.0.6
### 1.0.5-r1
* Update Node-RED to 1.0.5
### 1.0.4-r2
* Alpine 3.11.3
### 1.0.4-r1
* Update to Node-RED (1.0.4)
### 1.0.3-r2
* Update to Alpine 3.11.2
### 1.0.3-r1
* Update Node-Red (1.0.3)
### 1.0.2-r1
* Update Node-Red (1.0.2) and Alpine (3.10.3)
### 0.20.7-r4
* Update Node-Red (0.20.7) and Alpine (3.10.1)
### 0.20.7-r1
* Update Node-Red (0.20.7) and Alpine (3.10)
### 0.20.5-r1
* Update Node-Red (0.20.5)
### 0.20.4-r3
* Updated Node-Red (0.20.4) and Alpine (3.9)
### 0.19.5-r2
* Node-RED with remote git repository support
